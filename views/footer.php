<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Developed by Tentacular Technologies </b> <img src="<?php echo URL;?>views/images/tent_logo.png"> <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="http://almsaeedstudio.com">ABN</a>.</strong> All rights
    reserved.
  </footer>

  </div>
