<style>
/*Now the CSS*/
* {margin: 0; padding: 0;}

.tree ul {
	padding-top: 20px; position: relative;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

.tree li {
	float: left; text-align: center;
	list-style-type: none;
	position: relative;
	padding: 20px 5px 0 5px;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before, .tree li::after{
	content: '';
	position: absolute; top: 0; right: 50%;
	border-top: 1px solid #ccc;
	width: 50%; height: 20px;
}
.tree li::after{
	right: auto; left: 50%;
	border-left: 1px solid #ccc;
}

/*We need to remove left-right connectors from elements without 
any siblings*/
.tree li:only-child::after, .tree li:only-child::before {
	display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child{ padding-top: 0;}

/*Remove left connector from first child and 
right connector from last child*/
.tree li:first-child::before, .tree li:last-child::after{
	border: 0 none;
}
/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before{
	border-right: 1px solid #ccc;
	border-radius: 0 5px 0 0;
	-webkit-border-radius: 0 5px 0 0;
	-moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
	border-radius: 5px 0 0 0;
	-webkit-border-radius: 5px 0 0 0;
	-moz-border-radius: 5px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before{
	content: '';
	position: absolute; top: 0; left: 50%;
	border-left: 1px solid #ccc;
	width: 0; height: 20px;
}

.tree li a{
	border: 1px solid #ccc;
	padding: 5px 10px;
	text-decoration: none;
	color: #666;
	font-family: arial, verdana, tahoma;
	font-size: 11px;
	display: inline-block;
	
	border-radius: 5px;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	
	transition: all 0.5s;
	-webkit-transition: all 0.5s;
	-moz-transition: all 0.5s;
}

/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
.tree li a:hover, .tree li a:hover+ul li a {
	background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}
/*Connector styles on hover*/
.tree li a:hover+ul li::after, 
.tree li a:hover+ul li::before, 
.tree li a:hover+ul::before, 
.tree li a:hover+ul ul::before{
	border-color:  #94a0b4;
}

/*Thats all. I hope you enjoyed it.
Thanks :)*/
</style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="alert alert-warning alert-dismissible" style="text-align:center;">
                <h4>Motto: Adloyalty Business Network ...creating wealth, empowering people! </h4>
            <h4><a href="<?php echo URL.$user_details[0]['ref_code'];?>"><?php echo URL.$user_details[0]['ref_code']; ?></a></h4>
              </div>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $dow_line_count; ?></h3> 

              <p>Consuultants</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3></h3>

              <p>Commissions</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $client_count; ?></h3>

              <p>Clients</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      </section>
      
      
      
        <section class="content">
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Properties</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
           
                   <!--<div class="box-body">
              <table id="example2" class="table table-bordered table-hover" style="background: #fff;">
                <thead class='tables'>
                <tr>
                  <th>Picture</th>
                  <th>Property ID</th>
                  <th>Property Name</th>
                  <th>Description</th>
                  <th>Documents</th>
                  <th>Location</th>
                    <th>State</th>
                  
                  <th>price</th>
                  <th>Status</th>
                  
                  <th>Send details</th>
                </tr>
                </tr>
                </thead>
                
<?php foreach ($properties as $key => $value) {  ?>
<tr>
                   <td><img src="<?php echo $value['photo']; ?>" style="width:100%;"/></td>
                  <td><?php echo $value['prand']; ?>
                  </td>
                  <td><?php echo $value['property_name']; ?></td>
                  <td><?php echo $value['description']; ?></td>
                  <td><?php echo $value['document']; ?></td>
                  <td><?php echo $value['location']; ?></td>
                  <td><?php echo $value['state']; ?></td>
                  <td><?php echo $value['price']; ?>
                  </td>
                  <td><?php echo $value['status']==1? 'Active':'Sold'; ?></td>
                  
                  <td><a href="<?php echo  URL.'dashboard/smsselect?selects_property='.$value['property_id']; ?>">send</a></td>
                </tr>    

<?php

  
  
} ?>

                
                
            
                
                   
                
                
              </table>
            </div>-->
            <!-- /.box-body -->
                
            </div>

            
            <div class="grid">
				<?php foreach ($properties as $key => $value) {  ?>
                        <div class="property masonry">
                            <div class="inner">
                                <a href="property-detail.html">
                                    <div class="property-image">
                                        <figure class="tag status">For Sale</figure>
                                        <figure class="type" title="Apartment"><img src="<?php echo URL;?>public/assets/assets/img/property-types/apartment.png"  alt=""></figure>
                                        <div class="overlay">
                                            <div class="info">
                                                <div class="tag price">₦ <?php echo $value['price']; ?></div>
                                            </div>
                                        </div>
                                        <img alt="" src="<?php echo $value['photo']; ?>">
                                    </div>
                                </a>
                                <aside>
                                    <header>
                                        <a href="property-detail.html"><h3><?php echo $value['location']; ?></h3></a>
                                        <figure><?php echo $value['property_name']; ?></figure>
                                    </header>
                                    <p><?php echo $value['description']; ?></p>
                                    <dl>
                                        <dt>Status:</dt>
                                        <dd>Sale</dd>
                                        <dt>Area:</dt>
                                        <dd>860 m<sup>2</sup></dd>
                                        <dt>Beds:</dt>
                                        <dd>3</dd>
                                        <dt>Baths:</dt>
                                        <dd>2</dd>
                                    </dl>
                                    <a href="property-detail.html" class="link-arrow">Read More</a>
                                </aside>
                            </div>
                        </div>
				<?php } ?>
			</div>
</section>
      
      
      
      
    

             


 <section class="content">
         

                
            
                <div class="tree">
	<ul>
		<li>
			<a href="#">Parent</a>
			<ul>
				<li>
					<?php $i=1; foreach ($downline as $key => $value) {
    
  $val=$value['gen'];
  $gen=explode(',', $val);
  $keys = array_search($user_id, $gen);
  //echo $keys;
  if($keys===0){
    ?>
<a href="#"><?php echo $value['f_name'].' '.$value['l_name']; ?>1</a>
	<?
	  foreach ($downline as $key1 => $value1) {
		   $val2=$value1['gen'];
	  $gen2=explode(',', $val2);
	$user_id2 = $value['new_id'];
  $keys2 = array_search($user_id2, $gen2);
	  if($keys2===0){
	  ?>
	<ul>
						
						<li>
							<a href="#"><?php echo $value1['f_name'].' '.$value1['l_name']; ?>2</a>
							<ul>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
								<li>
									<a href="#">Great Grand Child</a>
								</li>
							</ul>
						</li>
						<li><a href="#">Grand Child</a></li>
					</ul>
	
	<?}}?>
	
       
<?php
$i++;
  }
  
} ?>
					
					
				</li>
			</ul>
		</li>
	</ul>
</div>
                   
                
                
             
         
                    
           
           
     
           
       
</section>
	  
	  
	  
	  
      </div>


<!--
We will create a family tree using just CSS(3)
The markup will be simple nested lists
-->

	
