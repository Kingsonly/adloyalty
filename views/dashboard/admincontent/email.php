<style>
.input-group, .form-control{margin-bottom:10px}
</style>
<div class="content-wrapper" ng-controller="sendEmailController">
<section class="content col-lg-10" style="margin:0 auto;float:none">
<h3>Send Email</h3>
<div class="box box-success">
<div class="register-box-body">

    	<div id="result" class="alert alert-info col-lg-5" style="float:none;display:none; margin:auto;text-align:center;margin-bottom:10px"></div>

    <div class="loader" style="text-align:center;margin-bottom:10px;display:none">
     <img src="<?php echo URL;?>views/images/load1.gif" style="margin:auto;width:40px" />
    </div>

    <form id="sendemail" ng-submit="sendemail()">

    <div class="col-lg-6">
      <div class="input-group has-feedback" style="">
      <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-user"></i> Sender ID</button>
      </div>
      <input type="text" name="sender_id" class="form-control pull-right col-sm-6" placeholder="Adloyalty email" required="required" value="noreply@adloyalty.com">
    </div>

     <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-group"></i> Send To Group</button>
      </div>
<select class="form-control select2" style="width: 100%;" name="users">
<option value="0">---Select Recipient --- </option>
<option value="tbl_consultant">All Consultant</option>
<option value="tbl_clients">All Clients</option>
</select>
    </div>

      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-goup"></i> Send to individual</button>
      </div>
       <input type="email" name="recipient_email" class="form-control pull-right" placeholder="Email address">
    </div>



   </div>


      <div class="col-lg-6">
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-goup"></i> Subject</button>
      </div>
       <input type="text" name="subject" class="form-control pull-right" placeholder="Message subject" required="required">
    </div>
	
	<div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-goup"></i></button>
      </div>
       <input type="file" name="fileatt" class="form-control pull-right" placeholder="Message subject">
    </div>

   <!--  <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> Message</button>
      </div>
      	<textarea class="form-control pull-right" placeholder="Message" required="required" style="width:100%"></textarea>
    </div> -->


    <input type="hidden" name="status" value="1">
     <input type="hidden" name="date" value="<?php echo date('Y-m-d');?>">
      </div>

        <div class="col-md-12" style="padding-top: 15px !important;">
         
              <div class="form-group">
                    <textarea id="compose-textarea" class="form-control" name="message" style="height: 300px">
                      
                      
                    </textarea>
              </div>
        </div>

    <div class="col-xs-4" style="padding-left:0">
     <button type="submit" class="btn btn-success btn-sm">SEND MESSAGE</button>
     </div>
      <div style="clear:both"></div>
    </form>
      <div style="clear:both"></div>
  </div>
  </div>
</section>
</div>
