<div class="content-wrapper" ng-controller="transactionsController">
<?php if(!isset($_GET['add'])){?>
<section class="content">
<h3>List of Transactions</h3>
      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Recent Payment(s)
              </h3>
              <div class="box-tools">

              <div class="input-group input-group-sm" style="width: 350px;float:right">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" ng-model="transactionSearch">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                   <a href="?add" class="btn btn-sm btn-info pull-right">Add</a>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table  table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th width="10px">Consultant Name</th>

                  <th width="130px">Client's Name & Details</th>
                  <th>Ref. ID</th>

                  <th>Amount Paid</th>
                  <th>Balance</th>
                  <th>Date of payment</th>
                  <td></td>
                </tr>
                <tr dir-paginate="transactions in transactions | filter:q | filter: transactionsSearch |  itemsPerPage: pageSize" current-page="currentPage"  ng-cloak>
                  <td>{{$index + 1}}</td>
                  <td width="150px">{{transactions.f_name}} {{transactions.l_name}}<br/>
                  <span style="font-size:14px;color:#bbb"><i class="fa fa-phone"></i> {{transactions.phones}}</i>
                  </td>

                  <td width="250px">{{transactions.fname}} {{transactions.lname}}<br/>
                  <span style="font-size:14px;color:#bbb"><i class="fa fa-home"></i> {{transactions.property_name}}</i><br/>
                  <i class="fa fa-money"></i> N{{transactions.price}}
                  </td>

                  <td>{{transactions.payment_rand}}</td>
                  <td>N{{transactions.amount_paid}}</td>
                  <td><strong> N{{transactions.price - transactions.amount_paid}}</strong></td>
                  <td>{{transactions.payment_date}}</td>
                  <td><span class="badge bg-green" ng-if="transactions.status=='1'">Complete</span>
                    <span class="badge bg-default" ng-if="transactions.status=='0'">Incomplete</span>
                    <a ng-if="transactions.status=='0'" href="http://{{dirlocation}}/admindashboard/transaction?update={{transactions.id}}"><span class="badge bg-info">Update</span></a>
                  </td>
                </tr>
              </table>
              <dir-pagination-controls boundary-links="true" template-url="<?php echo URL;?>views/dashboard/admincontent/dirPagination.tpl.html"></dir-pagination-controls>
            </div>

          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
 <?php }else{?>
<style>
.input-group, .form-control{margin-bottom:10px}
</style>

<section class="content">
<h3>New Transaction</h3>
<div class="box box-info">
<div class="register-box-body">

    	<div id="result" class="alert alert-info col-lg-5" style="float:none;display:none; margin:auto;text-align:center;margin-bottom:10px"></div>

    <div class="loader" style="text-align:center;margin-bottom:10px;display:none">
     <img src="<?php echo URL;?>views/images/load1.gif" style="margin:auto;width:40px" />
    </div>

    <form id="savetransaction" ng-submit="savetransaction()">

    <div class="col-lg-6">
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-user"></i> Client</button>
      </div>
                <select class="form-control select2" ng-init="client_id = '0'" style="width: 100%;" name="client_id" ng-model="client_id" ng-change="selectConsultant()">
                <option value="0">---Select Client --- </option>
                  <option ng-repeat="transactions in transactions" value="{{transactions.client_id}} ">{{transactions.fname}}   {{transactions.lname}}</option>
                </select>

    </div>

    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-user"></i> Consultant</button>
      </div>

                <select class="form-control select2" ng-init="consultant_id = '0'" style="width: 100%;" ng-model="consultant_id" name="consultant_id" ng-change="selectProperty()" id="consultant_id">
                <option value="0">---Select Consultant --- </option>
                  <option ng-repeat="consultant in consultant" value="{{consultant.id}}">{{consultant.f_name}}   {{consultant.l_name}}</option>
                </select>

    </div>

    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> Property</button>
      </div>
                <select class="form-control select2" style="width: 100%;" name="property_id">
                <option value="0">---Select Property --- </option>
                  <option ng-repeat="property in properties" value="{{property.property_id}} ">{{property.property_name}}   {{property.price}}</option>
                </select>

    </div>


   </div>


      <div class="col-lg-6">
    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-money"></i> Amount</button>
      </div>
      <input type="number" name="amount_paid" class="form-control pull-right" placeholder="Amount" required="required">

    </div>

    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-group"></i> Payment Method</button>
      </div>
                <select class="form-control select2" style="width: 100%;" name="payment_method">
                <option value="0">Cash </option>
                  <option value="1">Transfer</option>
                </select>

    </div>

     <input type="hidden" name="payment_date" value="<?php echo date('Y-m-d');?>">
    <input type="hidden" name="payment_rand" value="<?php echo rand(10000000,1000000000);?>">
     <input type="hidden" name="status" value="1">
    <div class="col-xs-4" style="padding-left:0">
     <button type="submit" class="btn btn-primary btn-sm">SUBMIT</button>
     </div>
      </div>


      <div style="clear:both"></div>
    </form>
      <div style="clear:both"></div>
  </div>
  </div>
</section>
<?php }?>
</div>
