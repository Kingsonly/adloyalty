<style>
#breathing-button {
    width: 28px;
    border: 1px solid #d1d1d1;
	background:red;
    -webkit-animation: breathing 7s ease-out infinite normal;
    animation: breathing 7s ease-out infinite normal;
    color: #fff;
    -webkit-font-smoothing: antialiased;
   
       
    }


@-webkit-keyframes breathing {
  0% {
    -webkit-transform: scale(0.9);
    transform: scale(0.9);
  }

  25% {
    -webkit-transform: scale(1);
    transform: scale(1);
  }

  60% {
    -webkit-transform: scale(0.9);
    transform: scale(0.9);
  }

  100% {
    -webkit-transform: scale(0.9);
    transform: scale(0.9);
  }
}

@keyframes breathing {
  0% {
    -webkit-transform: scale(0.9);
    -ms-transform: scale(0.9);
    transform: scale(0.9);
  }

  25% {
    -webkit-transform: scale(1);
    -ms-transform: scale(1);
    transform: scale(1);
  }

  60% {
    -webkit-transform: scale(0.9);
    -ms-transform: scale(0.9);
    transform: scale(0.9);
  }

  100% {
    -webkit-transform: scale(0.4);
    -ms-transform: scale(0.5);
    transform: scale(0.8);
  }
}

</style>
<div class="login-box">
   <div class="login-logo" style="font-size:30px">
      <img src="<?php echo URL;?>views/images/adloyalty_logo.png" style="width:70px";?><br/>
    <a href="../../index2.html"><b>Adloyalty</b> Business Network</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form action="login/run" method="post">
    <?php if(!empty($wrong_password)){?>    
    <div class="alert alert-danger"> 
    <?php echo $wrong_password;?> 
    </div>    
    <?php }?>   
        
    <?php if(!empty($activation)){?>    
    <div class="alert alert-danger"> 
    <?php echo $activation;?> 
    </div>    
    <?php }?>   
      <div class="form-group has-feedback">
        <input type="text" id="email" class="form-control" placeholder="Email" name="username" onkeydown="if (event.keyCode == 13) {return false;}" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
		  <div class="info"></div>
		  <div id="validator" class="btn btn-primary btn-block btn-flat">Validate Email</div>
		  
      </div>
		
		<div id="form1" style="display:none;">
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
	
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button  type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
		 </div>
        <!-- /.col -->
      </div>
		
		
		
	<div id="form2" style="display:none;">
      <div class="form-group has-feedback">
        <input type="input" class="form-control" id="code" placeholder="Your Ref Code" name="ref_code">
        <span id="breathing-button" style="cursor:pointer;pointer-events:auto !important;" class="glyphicon glyphicon-info-sign form-control-feedback" onclick="alert('Please Note: ref code should be your ref code and not that of your upline ')"></span>
      </div>
		
		<div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password2">
		<input type="hidden" id="newid" class="form-control"  name="newid">
			
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
	
      <div class="row">
		  <div id="codeinfo"></div>
        <!-- /.col -->
        <div class="col-xs-4">
			
          <button id="signin" type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
		 </div>
        <!-- /.col -->
      </div>
		
		
		
    </form>

    <!-- /.social-auth-links -->
      <br/>
    <!--<a href="forgotpassword">I forgot my password</a><br>-->
    <a href="registration" class="text-center">Register as  a new member.</a>
    <span style="cursor:pointer !important;" class="text-center"  data-toggle="modal" data-target="#myModal" >Forgot your password?</span>

  </div>
  <!-- /.login-box-body -->
</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Reset Password</h4>
      </div>
      <div class="modal-body">
        <p id="loader">   </p>
		  <div id="enteremail">
		  <form id="forgotpassword" method="post">
    
      <div class="form-group has-feedback">
        <input type="text" id="emails" class="form-control" placeholder="Email" name="email" onkeydown="if (event.keyCode == 13) {return false;}" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
		  
		  
      </div>
		
		<div>
      <div class="row">
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Reset Password</button>
        </div>
		 </div>
        <!-- /.col -->
      </div>
		
    </form>
	</div>
	
		  
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


</script>