<?php 
$uri=$_SERVER['REQUEST_URI'];
$explode=explode('/',$uri);
$end=strtolower(end($explode));
;?>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <body ng-app="mlmsoft" class="hold-transition skin-black sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>BN</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php echo URL;?>views/images/adloyalty_logo.png" style="width:20px";?> <b>Adloyalty</b> BN</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#"  class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav" >
        <li>
                  <a href="<?php echo URL; ?>admindashboard/logout/" class="btn btn-danger btn-flat"><i class = "fa fa-sign-out"></i></a></li>
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- User Account: style can be found in dropdown.less -->
      <!--     <li class="dropdown user user-menu" >
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              
              <span class="hidden-xs"><?php echo $this->session_details[0]['fname'].' '.$this->session_details[0]['lname'];?></span>
            </a>
            <ul class="dropdown-menu">

              <li class="user-header" style="height:auto;padding-bottom:0;">
                

                <p>
                  <?php echo $this->session_details[0]['fname'].' '.$this->session_details[0]['lname'];?> - Administrator
                  <small>Joined since <?php echo $this->session_details[0]['date'];?></small>
                </p>
              </li>

              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-info btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo URL; ?>admindashboard/logout/" class="btn btn-danger btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li> -->
          <!-- Control Sidebar Toggle Button -->
         
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
          
    <p style="font-weight:;color:#fff" class="text-center"><?php echo $this->session_details[0]['fname'].' '.$this->session_details[0]['lname'];?><br/>
          <small>Member since <?php echo $this->session_details[0]['date'];?></small>
          </p>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="<?php if($end=='admindashboard'){echo 'active';}?> treeview">
          <a href="<?php echo URL.'admindashboard';?>">
            <i class="fa fa-dashboard"></i> <span>Overview</span>
            
          </a>
        </li>
        <li class="<?php if($end=='consultant'){echo 'active';}?> treeview">
          <a href="<?php echo URL.'admindashboard/consultant';?>">
            <i class="fa fa-group"></i>
            <span>Consultants</span>
            
          </a>
          
        </li>
        <li class="<?php if($end=='clients'){echo 'active';}?> treeview">
          <a href="<?php echo URL.'admindashboard/clients';?>">
            <i class="fa fa-group"></i>
            <span>Clients</span>
            
          </a>
          
        </li>
        <li class="<?php if($end=='properties'){echo 'active';}?> treeview">
          <a href="<?php echo URL.'admindashboard/properties';?>">
            <i class="fa fa-home"></i> <span>Properties</span>
            
          </a>
        </li>
        <li class="<?php if($end=='transactions'){echo 'active';}?> treeview">
          <a href="<?php echo URL.'admindashboard/transactions';?>">
            <i class="fa fa-pie-chart"></i>
            <span>Transactions</span>
            
          </a>
        </li>
        <li class="<?php if($end=='commissions'){echo 'active';}?> treeview">
          <a href="<?php echo URL.'admindashboard/commissions';?>">
            <i class="fa fa-laptop"></i>
            <span>Commissions</span>
          </a>
          
        </li>
        
        <li class="<?php if($end=='bulksms'){echo 'active';}?> treeview">
          <a href="<?php echo URL;?>admindashboard/bulksms">
            <i class="fa fa-envelope"></i> <span>Bulk Sms</span>
            
          </a>
          
        </li>
        
         <li class="<?php if($end=='email'){echo 'active';}?> treeview">
          <a href="<?php echo URL;?>admindashboard/email">
            <i class="fa fa-envelope"></i> <span>Email</span>
            
          </a>
          
        </li>
        
         <li class="<?php if($end=='registerconsultant'){echo 'active';}?> treeview">
          <a href="<?php echo URL.'admindashboard/registerconsultant';?>">
            <i class="fa fa-pencil"></i> <span>Consultant's Registration</span>
          </a>
        </li>
        
        <li class="<?php if($end=='registerclient'){echo 'active';}?> treeview">
          <a href="<?php echo URL.'admindashboard/registerclient';?>">
            <i class="fa fa-pencil"></i> <span>Client's Registration</span>
            
          </a>  
        </li>
        
         
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  

  