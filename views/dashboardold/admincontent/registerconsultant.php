<style>
.input-group, .form-control{margin-bottom:10px}
</style>
<div class="content-wrapper" ng-controller="registerConsultantController">
<section class="content">
<h3>Register a New Consultant</h3>
<div class="box box-info">
<div class="register-box-body">

    	<div id="result" class="alert alert-info col-lg-5" style="float:none;display:none; margin:auto;text-align:center;margin-bottom:10px"></div>

    <div class="loader" style="text-align:center;margin-bottom:10px;display:none">
     <img src="<?php echo URL;?>views/images/load1.gif" style="margin:auto;width:40px" />
    </div>

    <form id="register" ng-submit="register()">

    <div class="col-lg-6">
      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-user"></i> First Name</button>
      </div>
      <input type="text" name="f_name" class="form-control pull-right" placeholder="First Name" required="required">

    </div>

     <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-user"></i> Last Name</button>
      </div>
      <input type="text" name="l_name" class="form-control pull-right" placeholder="Last Name" required="required">

    </div>

      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-envelope"></i> Email</button>
      </div>
      <input type="text" name="email" class="form-control pull-right" placeholder="Email Address" required="required">

    </div>

    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-lock"></i> Password</button>
      </div>
      <input type="password" name="password" class="form-control pull-right" placeholder="*******" required="required">
    </div>

    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-lock"></i> Retype Password</button>
      </div>
      <input type="password" name="retype_password" class="form-control pull-right" placeholder="*******" required="required">
    </div>

      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-phone"></i> Mobile</button>
      </div>
      <input type="text" name="phones" class="form-control pull-right" placeholder="Phone Number" required="required">

    </div>

      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i> DOB</button>
      </div>
      <input type="date" name="dob" class="form-control pull-right" placeholder="DOB">

    </div>


      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-link"></i> Referrer Link</button>
      </div>
      <input type="text" name="ref_code" class="form-control pull-right" placeholder="109433" required="required">

    </div>
   </div>


      <div class="col-lg-6">

        <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-gender"></i> Gender</button>
      </div>
      <select class="form-control" name="gender">
      <option value="1">Male</option>
      <option value="2">Female</option>
      </select>
    
    </div>

      	<textarea class="form-control pull-right" placeholder="Address" required="required" style="width:100%" name="address"></textarea>

      <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> City</button>
      </div>
      <input type="text" name="city" class="form-control pull-right" placeholder="City" required="required">
    </div>


    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> State</button>
      </div>
      <input type="text" name="state" class="form-control pull-right" placeholder="State" required="required">
    </div>


         <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-home"></i> Country</button>
      </div>
      <input type="text" name="country" class="form-control pull-right" placeholder="Country" required="required">
    </div>

    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="button" class="btn btn-default"><i class="fa fa-bank"></i> Banker</button>
      </div>
      <input type="text" name="bank" class="form-control pull-right" placeholder="Banker" required="required">

    </div>

    <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="text" class="btn btn-default"><i class="fa fa-user"></i> Account Name</button>
      </div>
      <input type="text" name="acc_name" class="form-control pull-right" placeholder="Account Name" required="required">
    </div>

     <div class="input-group has-feedback" style="">
     <div class="input-group-btn">
        <button type="number" class="btn btn-default"><i class="fa fa-th"></i> Account Number</button>
      </div>
      <input type="text" name="acc_num" class="form-control pull-right" placeholder="Account Number" required="required">
    </div>

    <input type="hidden" name="status" value="1">

    <div class="col-xs-4" style="padding-left:0">
           <button type="submit" class="btn btn-primary btn-sm">SUBMIT</button>
     </div>
      </div>
      <div style="clear:both"></div>
    </form>
      <div style="clear:both"></div>
  </div>
  </div>
</section>
</div>
