<div class="content-wrapper" ng-controller="consultantController">
<section class="content">

<h3>Consultants List</h3>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">List Of Consultants</h3>
              <a href="<?php echo URL;?>admindashboard/registerconsultant" class="btn btn-default">+ Register Consultant</a>
              <div class="box-tools">

              <div class="input-group input-group-sm" style="width: 350px;float:right">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" ng-model="consultantSearch">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>

                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table  table-striped">
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Name</th>
                  <th>Link</th>
                  <th>Phone Number</th>
                  <th>Bank Details</th>
                  <th style="width: 40px">Status</th>
                  <th style="width: 40px">&nbsp;</th>
                </tr>
                <tr dir-paginate="consultant in consultant | filter:q | filter: consultantSearch |  itemsPerPage: pageSize" current-page="currentPage" ng-cloak >
                  <td>{{$index + 1}}</td>
                  <td><strong><a href="http://{{dirlocation}}admindashboard/generation?getdetails={{consultant.id}}">{{consultant.f_name}} {{consultant.l_name}}</a></strong></td>
                  <td>
                   http://www.portal.adloyaltybn.com/{{consultant.ref_code}}
                  </td>
                  <td>{{consultant.phones}}</td>
                  <td>{{consultant.bank}}<br/>
                  <span style="font-size:12px;color:#aaa">{{consultant.acc_name}} :: {{consultant.acc_num}}</span>
                  </td>
                  <td><span class="badge bg-green" ng-if="consultant.status=='1'">Active</span>
                    <span class="badge bg-red" ng-if="consultant.status=='2'">Disabled</span>
                  <span class="badge bg-default" ng-if="consultant.status=='0'">Inactive</span></td>
                  <td><a href="http://{{dirlocation}}admindashboard/registerconsultant?getdetails={{consultant.id}}"><span class="badge bg-default">Edit</span></a></td>
                </tr>



              </table>
            </div>
            <!-- /.box-body -->
            <dir-pagination-controls boundary-links="true" template-url="<?php echo URL;?>views/dashboard/admincontent/dirPagination.tpl.html"></dir-pagination-controls>
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
</div>
