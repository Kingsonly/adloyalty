<div class="content-wrapper" ng-controller="clientsController">
<section class="content">

<h3>Clientele List</h3>

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">List Of Clients</h3>

              <div class="box-tools">

              <div class="input-group input-group-sm" style="width: 350px;float:right">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search" ng-model="clientSearch">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>

                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table  table-striped">
                <tr>
                  <th width="95" style="width: 10px">#</th>
                  <th width="213">Name</th>
                  <th width="100">Property</th>
                    <th width="100">Email</th>
                  <th width="102">Phone Number</th>
                  <th width="187" style="width: 40px">Address</th>
                  <th width="86" style="width: 40px">City</th>
                  <th width="93" style="width: 40px">State</th>
                  <th width="24"></th>
                </tr>
                <tr dir-paginate="client in clients | filter:q | filter: clientSearch |  itemsPerPage: pageSize" current-page="currentPage" ng-cloak>
                  <td>{{$index + 1}}</td>
                  <td><strong><a href="http://{{dirlocation}}admindashboard/transactions?getdetails={{client.id}}">{{client.fname}} {{client.lname}}</a></strong></td>
                  <td>
                   {{client.property_name}}
                  </td>
                  <td>
                   {{client.email}}
                  </td>
                  <td>{{client.phone}}</td>
                  <td>{{client.address}}</td>
                   <td>{{client.city}}</td>
                    <td>{{client.state}}</td>
                  <td><a href="http://{{dirlocation}}/mlmsoft/admindashboard/registerclient?getdetails={{client.id}}"><span class="badge bg-default">Edit</span></a></td>
                </tr>



              </table>
            </div>
            <!-- /.box-body -->
            <dir-pagination-controls boundary-links="true" template-url="<?php echo URL;?>views/dashboard/admincontent/dirPagination.tpl.html"></dir-pagination-controls>
          </div>
          <!-- /.box -->


          <!-- /.box -->
        </div>
        <!-- /.col -->

        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
</div>
