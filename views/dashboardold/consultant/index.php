<!-- Content Wrapper. Contains page content -->
<style>
    .tables{
        background: #3C8DBC;
        color: #fff;
    }
</style>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="alert alert-warning alert-dismissible" style="text-align:center;">
                <h4>Motto: Adloyalty Business Network ...creating wealth, empowering people! </h4>
            <h4><a href="<?php echo URL.$user_details[0]['ref_code'];?>"><?php echo URL.$user_details[0]['ref_code']; ?></a></h4>
              </div>
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo $dow_line_count; ?></h3> 

              <p>Consuultants</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3></h3>

              <p>Commissions</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-12">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?php echo $client_count; ?></h3>

              <p>Clients</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      </section>
      
      
      
        <section class="content">
            <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Properties</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            
                   <div class="box-body">
              <table id="example2" class="table table-bordered table-hover" style="background: #fff;">
                <thead class='tables'>
                <tr>
                  <th>Picture</th>
                  <th>Property ID</th>
                  <th>Property Name</th>
                  <th>Description</th>
                  <th>Documents</th>
                  <th>Location</th>
                    <th>State</th>
                  
                  <th>price</th>
                  <th>Status</th>
                  
                  <th>Send details</th>
                </tr>
                </tr>
                </thead>
                
<?php foreach ($properties as $key => $value) {  ?>
<tr>
                   <td><img src="<?php echo $value['photo']; ?>" style="width:100%;"/></td>
                  <td><?php echo $value['prand']; ?>
                  </td>
                  <td><?php echo $value['property_name']; ?></td>
                  <td><?php echo $value['description']; ?></td>
                  <td><?php echo $value['document']; ?></td>
                  <td><?php echo $value['location']; ?></td>
                  <td><?php echo $value['state']; ?></td>
                  <td><?php echo $value['price']; ?>
                  </td>
                  <td><?php echo $value['status']==1? 'Active':'Sold'; ?></td>
                  
                  <td><a href="<?php echo  URL.'dashboard/smsselect?selects_property='.$value['property_id']; ?>">send</a></td>
                </tr>    

<?php

  
  
} ?>

                
                
            
                
                   
                
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
    
</section>
      
      
      
      
    

             
      
      

       <section class="content">


 <section class="content">
     <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">GENERATION</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         
         

      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-group" id="accordion">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel box ">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                        FIRST GENERATION
                      </a>
                    </h4>
                  </div>
                  <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="box-body">
                             
         
       <div class="box-body">
              <table  class="table table-bordered table-hover example2" style="background: #fff;">
                <thead class='tables'>
                <tr>
                    <th>Date of Reg</th>
                  <th>NAME</th>
                  <th>EMAIL</th>
                  <th>Mobile No </th>
                    
                    <th>CID</th>
                    <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                  
<?php $i=1; foreach ($downline as $key => $value) {
    
  $val=$value['gen'];
  $gen=explode(',', $val);
  $keys = array_search($user_id, $gen);
  //echo $keys;
  if($keys===0){
    ?>
<tr>
                 <td><?php echo $i; ?></td>
                  <td><?php echo $value['f_name'].' '.$value['l_name']; ?>
                  </td>
                  <td><?php echo $value['email']; ?></td>
                  <td><?php echo $value['phones']; ?></td>
                  <td><?php echo $value['new_id']; ?></td>
                  <td><?php echo isset($value['status'])==1?'Active':'Activation Pending'; ?></td>
                  <td><a href="<?php echo URL.'dashboard/downline_select?downline_select='.$value['new_id']; ?>">view doenline</a></td>
                </tr>    
<?php
$i++;
  }
  
} ?>

                
                
            
                
                   
                
                
              </table>
            </div>
                    </div>
                  </div>
                </div>
                <div class="panel box box-danger">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                        SECOND GENERATION
                      </a>
                    </h4>
                  </div>
                  <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="box-body">
                              
         
       <div class="box-body">
              <table  class="table table-bordered table-hover example2" style="background: #fff;">
                <thead class='tables'>
                <tr>
                 <th>Date of Reg</th>
                  <th>NAME</th>
                  <th>EMAIL</th>
                  <th>Mobile No </th>
                    
                    <th>CID</th>
                    <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                  
<?php $i=1; foreach ($downline as $key => $value) {
    
  $val=$value['gen'];
  $gen=explode(',', $val);
  $keys = array_search($user_id, $gen);
  //echo $keys;
  if($keys==1){
    ?>
<tr>
                  <td><?php echo $i; ?></td>
                  <td><?php echo $value['f_name'].' '.$value['l_name']; ?>
                  </td>
                  <td><?php echo $value['email']; ?></td>
                  <td><?php echo $value['phones']; ?></td>
                  <td><?php echo $value['id']; ?></td>
                  <td><?php echo isset($value['status'])==1?'Active':'Activation Pending'; ?></td>
                  <td><a href="<?php echo URL.'dashboard/downline_select?downline_select='.$value['new_id']; ?>">view doenline</a></td>
                </tr>    
                  

<?php
$i++;
  }
  
} ?>

                
                
            
                
                   
                
                
              </table>
            </div>
                    </div>
                  </div>
                </div>
                <div class="panel box box-success">
                  <div class="box-header with-border">
                    <h4 class="box-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                        THIRD GENERATION
                      </a>
                    </h4>
                  </div>
                  <div id="collapseThree" class="panel-collapse collapse">
                    <div class="box-body">
                              
         
       <div class="box-body">
              <table  class="table table-bordered table-hover example2" style="background: #fff;">
                <thead class='tables'>
                <tr>
                  <th>SN</th>
                  <th>NAME</th>
                  <th>EMAIL</th>
                  <th>Mobile No </th>
                   
                    <th>CID</th>
                    <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                  
<?php $i=1; foreach ($downline as $key => $value) {
    
  $val=$value['gen'];
  $gen=explode(',', $val);
  $keys = array_search($user_id, $gen);
  //echo $keys;
  if($keys==2){
    ?>
<tr>
                 <td><?php echo $i; ?></td>
                  <td><?php echo $value['f_name'].' '.$value['l_name']; ?>
                  </td>
                  <td><?php echo $value['email']; ?></td>
                  <td><?php echo $value['phones']; ?></td>
                  <td><?php echo $value['id']; ?></td>
                  <td><?php echo isset($value['status'])==1?'Active':'Activation Pending'; ?></td>
                  <td><a href="<?php echo URL.'dashboard/downline_select?downline_select='.$value['new_id']; ?>">view doenline</a></td>
                </tr>    

<?php
$i++;
  }
  
} ?>

                
                
            
                
                   
                
                
              </table>
            </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        
      </div>
         
         
         
         
         
         
         
         
         
         
         
         
         
 
     </div>
</section>
           
           

                
<!--<?php $i=1; foreach ($transactions as $key => $value) {  ?>
<tr>
                   <td><?php echo $i; ?></td>
                  <td><?php echo $value['amount']; ?>
                  </td>
                  <td><?php echo $value['dates']; ?></td>
                  <td><?php echo $value['status']==1? 'Pending':'Paid'; ?></td>
                  <td><?php echo 'view ticket'; ?></td>
                  
                </tr>    

<?php $i++ ;} ?>

                
                
            
                
                   
                
                
              </table>
            </div>
    </div>
</section>-->
      
      
           
           
     
           
       
</section>
      </div>

	
