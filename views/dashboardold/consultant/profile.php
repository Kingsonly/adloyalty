<style>
#targetLayer{
float:left;
width:100px;
height:100px;
text-align:center;
line-height:100px;
font-weight: bold;
color: #C0C0C0;
background-color: #F0E8E0;
overflow:auto;
}
#uploadFormLayer{
float:right;
padding: 10px;
}
.btnSubmit {
background-color: #3FA849;
padding:4px;
border: #3FA849 1px solid;
color: #FFFFFF;
}
.inputFile {
padding: 3px;
background-color: #FFFFFF;
}

	h3{
		color: #eb9e51;
	}
</style>
  <div class="content-wrapper"> 
     
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php if(file_exists($user_details[0]['pic_path'])){echo URL.$user_details[0]['pic_path'];}else{if($user_details[0]['gender']==1){echo URL.'public/dist/img/avatar.png';}else{ echo URL.'public/dist/img/avatar2.png';}}?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $user_details[0]['f_name'].' '.$user_details[0]['l_name'];?></h3>

              

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Consultants</b> <a class="pull-right"><?php echo Session::get('count') ?></a>
                </li>
                <li class="list-group-item">
                  <b>Clients</b> <a class="pull-right"><?php echo $clientcount; ?></a>
                </li>
                <li class="list-group-item">
                  <b>Total Commision</b> <a class="pull-right"><?php echo $commision; ?></a>
                </li>
                  
                  <!--<li class="list-group-item">
                  <b>Account balance</b> <a class="pull-right"><?php echo $user_details[0]['in_account'];?></a>
                </li>-->
                  
                  <li class="list-group-item">
                  <b>Gender</b> <a class="pull-right"><?php echo $user_details[0]['gender']==1?'Male':'Female';?></a>
                </li>
                  
                  <li class="list-group-item">
                  <b>Date of birth</b> <a class="pull-right"><?php echo $user_details[0]['dob'];?></a>
                </li>
              </ul>

              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
			
			<div class="box box-solid bg-black-gradient">
            <div class="box-header">
             

              <h3 class="box-title">Change profile picture</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-12">
                  <form class="form-horizontal" id="uploadForm" action="" method="post" enctype="multipart/form-data">
                  
                      <div style="height:100px"> 
                   
                        <div id="targetLayer">No Image</div>
                        <div id="uploadFormLayer">
                        <label>Upload Image File:</label><br/>
                        <input name="userImage" type="file" class="inputFile" />
                        <input  type="submit" value="Submit" class="btnSubmit" />

                        
                  </div>
                          </div>
                      </form>
                 
                </div>
                <!-- /.col -->
                
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
			
			
			<div class="box box-solid bg-black-gradient">
            <div class="box-header">
             

              <h3 class="box-title">Change Password</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-12">
                  	<form id="updatepwd" action="" method="post">
                   
                    <table class="table">
                        <tr>
                            
                            
                            <td  colspan="2">
                            <input type="password" id="pwd" name="password"/>
                            </td>
                        </tr> 
                        <tr>
                        <td colspan="2">
                          <input type="submit" class="btn btn-primary btn-block btn-flat" id="pwd_change" value="Change Password" /> 
                        <div id="tagetpwd"></div>
                        </td>
                        </tr>
                    </table>
                 </form>
                 
                </div>
                <!-- /.col -->
                
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
			
			
			
			
			<div class="box box-solid bg-black-gradient">
            <div class="box-header">
             

              <h3 class="box-title">Change Address</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-12">
                   <form id="address" action="" method="post" >
                    
                    <table class="table">
                        <tr>
                            
                            
                            <td  colspan="2">
                            <input type="text" name="address" id="addresss"/>
                            </td>
                        </tr> 
                        <tr>
                        <td colspan="2">
                          <input class="btn btn-primary btn-block btn-flat" type="submit" id="address_change" value="Change Address" /> 
                        <div id="tagetadress"></div>
                        </td>
                        </tr>
                    </table>
                 </form>
                  
                </div>
                <!-- /.col -->
                
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
			
			
			
			
			<div class="box box-solid bg-black-gradient">
            <div class="box-header">
             

              <h3 class="box-title">Change Account number</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <!-- button with a dropdown -->
                
                <button type="button" class="btn btn-black btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-black btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div id="calendar" style="width: 100%"></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black">
              <div class="row">
                <div class="col-sm-12">
                   <form id="accounts" action="" method="post" >
                    
                    <table class="table">
                        <tr>
                            
                            
                            <td  colspan="2">
                            <input type="text" name="account_number" id="account_number" placeholder="Account Number"/>
								<br/>
							<input type="text" name="bank_name" placeholder="Bank Name" id="bank_name"/>
                            </td>
                        </tr> 
                        <tr>
                        <td colspan="2">
                          <input class="btn btn-primary btn-block btn-flat" type="submit" id="account_change" value="Change Account Details" /> 
                        
                        </td>
                        </tr>
                    </table>
                 </form>
                  
                </div>
                <!-- /.col -->
                
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
			
			
			
			
			
			
			
			
			
          
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    
    <!-- /.content -->
  
           
           
           
           
</section>
      </div>


