<?php

class View {
public $data = array();
	function __construct() {
		//echo 'this is the view';
		//$this->view->logged = Session::get('loggedIn');
		//$this->view->loggedType = Session::get('loggedType');

	}

	public function render($name, $noInclude, $message)
	{
		if ($noInclude == true) {

			extract($this->data);
			require 'views/snipets/headerref.php';
			require 'views/' . $name . '.php';
			require 'views/snipets/footerref.php';		
		}
		else {
			extract($this->data);
			require 'views/snipets/headerref.php';
			//var_dump($this->view->loggedType);
			if(Session::get('loggedType') == 'consultant'){

				require 'views/consult_header.php';
			}else{
			require 'views/header.php';
		}
			require 'views/' . $name . '.php';
			require 'views/footer.php';	
			require 'views/snipets/footerref.php';	
		}
	}



}


